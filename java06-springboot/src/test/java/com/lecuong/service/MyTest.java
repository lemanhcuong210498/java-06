package com.lecuong.service;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        OperationTest.class
})
public class MyTest {

    /*
        Thông thường 1 class test sẽ sử dụng để test cho một chức năng, một unit. Vậy nếu muốn chạy nhiều class test để xem kết quả thì như nào?
        Câu trả lời là test suite, ta sẽ tạo một bộ gồm nhiều class để thực hiện test và xem kết quả sau một lần chạy.
        Để tạo test suite ta sử dụng

        @RunWith(Suite.class)và @SuiteClasses(TestClass1.class, ...). Bên trong @SuiteClasses sẽ là các class test được chạy.
     */
}
