package com.lecuong.service;

import com.lecuong.modal.Operation;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class OperationTest {

    private Operation operation;

    @Before
    public void setUp() throws Exception {
        operation = new Operation();
    }

    @Test
    public void testAdd() {
        int result = operation.add(2, 3);
        Assert.assertEquals(5, result);
    }

    @Test
    public void testMinus() {
        int result = operation.minus(5, 3);
        Assert.assertEquals(2, result);
    }

    @Test
    public void testOperationNotNull() {
        Assert.assertNotNull("Object not null", operation);
    }

    @After
    public void tearDown() throws Exception {
        System.out.println("Test successfully");
    }
}
